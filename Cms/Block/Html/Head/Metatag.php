<?php

/**
 * Copyright © Híbrido All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Hibrido\Cms\Block\Html\Head;

use Magento\Cms\Api\Data\PageInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Locale\ResolverInterface;
use Magento\Cms\Helper\Page as HelperPage;

/**
 * Class Metatag
 * @package Hibrido\Cms\Block\Html\Head
 */
class Metatag extends Template
{
    /**
     * @var PageInterface
     */
    private $cmsPage;

    /**
     * @var ResolverInterface
     */
    private $localeResolver;

    /**
     * @var HelperPage
     */
    private $helperPage;

    /**
     * Metatag constructor.
     * @param Context $context
     * @param PageInterface $cmsPage
     * @param ResolverInterface $localeResolver
     * @param HelperPage $helperPage
     * @param array $data
     */
    public function __construct(
        Context $context,
        PageInterface $cmsPage,
        ResolverInterface $localeResolver,
        HelperPage $helperPage,
        array $data = []
    ) {
        $this->cmsPage = $cmsPage;
        $this->localeResolver = $localeResolver;
        $this->helperPage = $helperPage;
        parent::__construct($context, $data);
    }

    /**
     * @return bool
     */
    public function checkMultipleStores(): bool
    {
        if (count($this->cmsPage->getStores()) > 1) {
            return true;
        }

        return false;
    }

    /**
     * @return string
     */
    public function getLanguage(): string
    {
        return str_replace('_', '-', strtolower($this->localeResolver->getLocale()));
    }

    /**
     * @return string
     */
    public function getCmsPageUrl(): string
    {
        return $this->helperPage->getPageUrl($this->cmsPage->getId());
    }
}
